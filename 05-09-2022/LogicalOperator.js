const x = 10;
const y = 100;

// Logical Operator AND & OR
console.log(1 && 10); // Ouput 10
console.log(0 || 100); // Ouput 100
console.log(true && false); // Ouput false
console.log(true && false || "Semangat Teruss"); // Output Semangat Terus
if (x * y % 100 === 0) {
    console.log("Bismillah Lancar!"); // Ouput Bismillah Lancar!
}