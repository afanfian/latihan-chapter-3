const x = 10;
const y = 100;

const NilaiLebihBesar = x >= 10;

const dataDiri = {
    nama: 'fian awamiry maulana',
    jenisKelamin: 'laki-laki',
    umur: 21
}
// const iniPerempuan = dataDiri.jenisKelamin === 'laki-laki'; Output true karena laki -  laki
const iniPerempuan = dataDiri.jenisKelamin === 'perempuan'; //Ouput false karena perempuan
console.log(iniPerempuan);
// const iniUmur = dataDiri.umur < 20; // Ouput false
const iniUmur = dataDiri.umur > 20; // Ouput true
console.log(iniUmur);

// Logical NOT
if (iniPerempuan) {
    console.log('yah ${dataDiri.nama} itu lelaki');
} else {
    console.log('${dataDiri.nama} itu perempuan');
}
