// Percobaan 1
// var a = 12
// {
//     a = 50
// }
// console.log(a);

// Percobaan 2
// a = 20
// {
//     let b = 12
// }
// console.log(b);

// Percobaan 3
// const a = {
//     nama: "Fian",
//     kelas: "Binar Academy batch 3"
// }
// a.nama = "Afan";
// console.log(a.nama);

// Percobaan 4 (Print Undefined)
// const a = {
//     nama: "Fian",
//     kelas: "Binar Academy batch 3"
// }
// let b = "ITS";
// a[b] = b;
// console.log(a.b);

// Percobaan 5 (Print ITS)
// const a = {
//     nama: "Fian",
//     kelas: "Binar Academy batch 3"
// }
// let b = "ITS";
// a[b] = b;
// console.log(a["ITS"]);

// Percobaan 6
// const a = {
//     nama: "afanfian",
//     kelas: "IMK",
//     "jenisOrang": "Jawa",
//     lari: () => console.log("futsal")
// }
// let b = "Fian Awamiry Maulana";
// a[b] = "Afanfian";
// console.log(a[jenisOrang])

// namaOrang = camelcase
// nama_orang = snakecase
// NamaOrang = Pummelcase
// namaorang = lowercase
// NAMAORANG = uppercase

// Percobaan 7 (Hoisting)
// var nama;
// nama = 'afanfian';
// console.log(nama);

// Percobaan 8
// const a = "Afanfian";
// a = "Imam";
// const b = {
//     nama: "muel",
//     nama2: "afan"
// }
// console.log(b.nama, b.nama2 = 2);
